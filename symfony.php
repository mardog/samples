<?php

require_once(__DIR__ . '/vendor/autoload.php');

use somethingXXX\Core\Context;
use somethingXXX\Core\ServiceContainer;
use somethingXXX\Core\Traits\ConfigurationAware;
use somethingXXX\Core\Traits\InstallAware;
use somethingXXX\Core\Traits\ServiceContainerAware;
use somethingXXX\somethingXXX\Configuration;
use somethingXXX\somethingXXX\Model\CustomerPermission;
use somethingXXX\somethingXXX\Tabs\CaptchaTab;
use somethingXXX\somethingXXX\Tabs\ConfirmationLinkTab;
use somethingXXX\somethingXXX\Tabs\GroupsTab;
use somethingXXX\somethingXXX\Tabs\ManagementTab;
use somethingXXX\somethingXXX\Tabs\PrivateTab;
use somethingXXX\somethingXXX\Tabs\ValidationTab;

class somethingXXX extends \Module
{
    use ServiceContainerAware, InstallAware, ConfigurationAware;

    private static $exceptions = array(
        'authentication',
        'password',
        'registered',
        'sent'
    );

	public function __construct()
	{
		$this->version = '3.0.0';
		$this->name = 'abc';
		$this->author = 'xxx.com';
		$this->module_key = '';
		$this->bootstrap = true;

        $this->ps_versions_compliancy = [
            'min' => '1.7',
            'max' => _PS_VERSION_
        ];

		parent::__construct();

        $this->displayName = $this->trans('Registration', [], 'Modules.somethingXXX.Admin');
        $this->description = $this->trans(
            'Module for registration  customers to the groups!',
            [],
            'Modules.somethingXXX.Admin'
        );

        $this->initializeServiceContainer();
	}

    /* INITIALIZATION */

    /**
     * Register hooks that need to be installed with the module.
     */
    protected function initializeHooks()
    {
        return [
            'displayAuthenticateFormBottom',
            'displayCreateAccountEmailFormBottom',

            'validateCustomerFormFields',

            'additionalCustomerFormFields',

            'actionBeforeAuthentication',
            'actionBeforeSubmitAccount',

            'actionCustomerAccountAdd',

            'displayHeader',

            'displayCustomerAccountFormTop',
            'displayCustomerAccountForm',

            'displayAdminCustomers',
        ];
    }

    /**
     * Register backoffice tabs that need to be installed with the module.
     */
    protected function initializeAdminTabs()
    {
        $this->tabs = [
            [
                'name' => $this->trans('Pending customers', [], 'Modules.somethingXXX.Admin'),
                'class_name' => 'AdminCustomersPermission',
                'ParentClassName' => 'AdminParentCustomer',
                'icon' => '',
                'visible' => true,
            ]
        ];
    }

    /**
     * Register configuration tabs that need to be installed with the module.
     */
    protected function initializeTabs()
    {
        $context = $this->getContext();
        $configuration = $this->getConfiguration();
        $tools = $this->getTools();

        return [
            new ManagementTab($context, $configuration, $tools),
            new ValidationTab($context, $configuration, $tools),
            new GroupsTab($context, $configuration, $tools),
            new ConfirmationLinkTab($context, $configuration, $tools),
            new CaptchaTab($context, $configuration, $tools),
            new PrivateTab($context, $configuration, $tools),
        ];
    }

    /**
     * Initializes service container with base classes.
     */
    protected function initializeServiceContainer()
    {
        $context = new Context($this, $this->context);
        $configuration = new Configuration($context);

        $this->setServiceContainer(new ServiceContainer(
            $context,
            $configuration
        ));
    }

    /* HOOKS */

    /**
     * @param $params
     * @return bool
     */
    public function hookAdditionalCustomerFormFields($params)
    {
        return $this->_showAccountForm(Configuration::POSITION_TOP);
    }

    public function hookValidateCustomerFormFields($params)
    {
        $configuration = $this->getConfiguration();

        $groups_management_enabled = $configuration->getValue(Configuration::GROUPS_MANAGEMENT_ENABLED);

        if (!$groups_management_enabled || !Group::isFeatureActive()) {
            return;
        }

        $groups_default_group = $configuration->getValue(Configuration::GROUPS_DEFAULT_GROUP);
        $groups_groups_to_select = $configuration->getValue(Configuration::GROUPS_TO_SELECT);

        $id_group = (int)$params['fields'][0]->addError();
        $id_default_group = (int)$groups_default_group;

        $selected_groups = (array)$groups_groups_to_select;
        $selected_groups[] = $id_default_group;

        $id_groups = $this->getTools()->getValue('id_groups');
        $id_groups = is_array($id_groups) ? array_diff($id_groups, $selected_groups) : array();

        if (empty($id_group)) {
            $this->context->controller->errors[] = $this->l('Account type is required');
        } elseif (!in_array($id_group, $selected_groups)) {
            $this->context->controller->errors[] = $this->l('Please select valid group');
        }

        if (!empty($id_groups)) {
            $this->context->controller->errors[] = $this->l('Please select valid groups');
        }

        var_dump($params['fields']);exit;
    }

    protected function _showAccountForm($position)
    {
        $configuration = $this->getConfiguration();

        $groups_management_enabled = $configuration->getValue(Configuration::GROUPS_MANAGEMENT_ENABLED);
        $groups_position = $configuration->getValue(Configuration::GROUPS_POSITION);

        if (!$groups_management_enabled || !\Group::isFeatureActive() || $groups_position != $position) {
            return [];
        }

        $groups_default_group = $configuration->getValue(Configuration::GROUPS_DEFAULT_GROUP);
        $groups_groups_to_select = $configuration->getValue(Configuration::GROUPS_TO_SELECT);

        $groups_section_name = $configuration->getValue(Configuration::GROUPS_SECTION_NAME);

        $groups_field_type = $configuration->getValue(Configuration::GROUPS_FIELD_TYPE);
        $groups_field_name = $configuration->getValue(Configuration::GROUPS_FIELD_NAME);
        $groups_field_description = $configuration->getValue(Configuration::GROUPS_FIELD_DESCRIPTION);

        $groups_default_name = $configuration->getValue(Configuration::GROUPS_FIELD_DEFAULT_NAME);

        $id_lang = $this->context->language->id;
        $id_default_group = (int)$groups_default_group;

        $selected_groups = (array)$groups_groups_to_select;
        $selected_groups[] = (int)$groups_default_group;

        $shop_groups = CustomerPermission::getGroups($this->context->language->id, true, true);
        $shop_groups = array_intersect_key($shop_groups, array_flip($selected_groups));
        $shop_groups = array_diff_key($shop_groups, array_flip(array(
            $id_default_group
        )));

        if (in_array((int)$groups_field_type, array(
            Configuration::TYPE_RADIO,
            Configuration::TYPE_SELECT
        ))) {
            $shop_groups[] = array(
                'id_group' => $id_default_group,
                'name' => !empty($groups_default_name[$id_lang]) ? $groups_default_name[$id_lang] : $this->trans('Other')
            );
        }

        $this->context->smarty->assign(array(
            'privilegeAccess' => ($groups_management_enabled ? true : false),
            'defaultGroup' => $id_default_group,
            'groups' => $shop_groups,

            'section_name' => !empty($groups_section_name[$id_lang]) ? $groups_section_name[$id_lang] : $this->l('Account group'),
            'field_type' => (int)$groups_field_type,
            'field_name' => !empty($groups_field_name[$id_lang]) ? $groups_field_name[$id_lang] : $this->l('Your group'),
            'field_description' => !empty($groups_field_description[$id_lang]) ? $groups_field_description[$id_lang] : $this->l('Select group you want to be assigned to')
        ));

        $fieldType = null;
        $fieldName = null;
        $fieldLabel = !empty($groups_field_name[$id_lang]) ? $groups_field_name[$id_lang] : $this->l('Your group');

        switch($groups_field_type) {
            case Configuration::TYPE_RADIO:
                $fieldType = 'radio-buttons';
                $fieldName = 'id_group';
                break;
            case Configuration::TYPE_CHECKBOX:
                $fieldType = 'checkbox';
                $fieldName = 'id_groups';
                break;
            case Configuration::TYPE_SELECT:
                $fieldType = 'select';
                $fieldName = 'id_group';
                break;
            case Configuration::TYPE_MULTISELECT:
                $fieldType = 'select';
                $fieldName = 'id_groups';
                break;
        }

        $formField = new FormField();
        $formField->setName($fieldName);
        $formField->setType($fieldType);
        $formField->setLabel($fieldLabel);
        $formField->setRequired(true);

        foreach ($shop_groups as $shop_group) {
            $formField->addAvailableValue($shop_group['id_group'], $shop_group['name']);
        }

        return [
            $formField,
        ];
    }

    protected function _validateAccountForm()
    {
        $configuration = $this->getConfiguration();

        $groups_management_enabled = $configuration->getValue(Configuration::GROUPS_MANAGEMENT_ENABLED);

        if (!$groups_management_enabled || !Group::isFeatureActive()) {
            return;
        }

        $groups_default_group = $configuration->getValue(Configuration::GROUPS_DEFAULT_GROUP);
        $groups_groups_to_select = $configuration->getValue(Configuration::GROUPS_TO_SELECT);

        $id_group = (int)$this->getTools()->getValue('id_group', 0);
        $id_default_group = (int)$groups_default_group;

        $selected_groups = (array)$groups_groups_to_select;
        $selected_groups[] = $id_default_group;

        $id_groups = $this->getTools()->getValue('id_groups');
        $id_groups = is_array($id_groups) ? array_diff($id_groups, $selected_groups) : array();

        if (empty($id_group)) {
            $this->context->controller->errors[] = $this->l('Account type is required');
        } elseif (!in_array($id_group, $selected_groups)) {
            $this->context->controller->errors[] = $this->l('Please select valid group');
        }

        if (!empty($id_groups)) {
            $this->context->controller->errors[] = $this->l('Please select valid groups');
        }
    }
}
